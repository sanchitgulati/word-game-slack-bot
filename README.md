# Node JS based Game Bot for Slack #

Bot will give you four letters everytime, you just keep making words to increase your score.

## Installation ##

```
#!bash

npm install @slack/client --save
npm install mathjs --save
npm install hashmap --save
```


## Set Up ##

You need to setup WORD_RUSH_SLACK_BOT environment variable to API Token assigned by slack.

## All Done ##


```
#!bash

node bot.js 
nohup node bot.js & // if you want to run this as background task
```