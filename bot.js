var words = ["abet", "able", "ably", "aced", "aces", "ache", "acid", "acme", "acne", "acre", "acts", "adds", "adze", "afar", "afro", "aged", "ages", "aide", "aids", "aims", "airs", "airy", "ajar", "akin", "alas", "ally", "alma", "alms", "aloe", "also", "alum", "amen", "amid", "ammo", "amok", "anal", "anew", "anon", "ante", "anti", "ants", "anus", "aped", "apes", "apex", "aqua", "arch", "arcs", "area", "arid", "arms", "army", "arts", "arty", "asks", "atom", "atop", "aunt", "aura", "auto", "avid", "avow", "away", "awed", "awry", "axed", "axes", "axil", "axis", "axle", "babe", "baby", "back", "bade", "bags", "bail", "bait", "bake", "bald", "bale", "bale", "balk", "ball", "balm", "band", "bane", "bang", "bank", "bans", "barb", "bard", "bare", "barf", "bark", "barn", "bars", "base", "bash", "bask", "bass", "bate", "bath", "bats", "bawl", "bays", "bead", "beak", "beam", "bean", "bear", "beat", "beau", "beck", "beds", "beef", "been", "beep", "beer", "bees", "beet", "begs", "bell", "belt", "bend", "bent", "best", "beta", "bets", "bevy", "bias", "bibs", "bide", "bids", "bier", "bike", "bile", "bilk", "bill", "bind", "bing", "bins", "bird", "bite", "bits", "blab", "blah", "bled", "blew", "blip", "blob", "bloc", "blog", "blot", "blow", "blue", "blur", "boar", "boat", "bobs", "bode", "body", "bogs", "boil", "bold", "bolt", "bomb", "bond", "bone", "bony", "boob", "book", "boom", "boon", "boor", "boos", "boot", "bore", "born", "boss", "both", "bout", "bowl", "bows", "boys", "brad", "brag", "bran", "bras", "brat", "bray", "bred", "brew", "brig", "brim", "brow", "buck", "buds", "buff", "bugs", "bulb", "bulk", "bull", "bump", "bums", "bunk", "buns", "bunt", "buoy", "burn", "burp", "bury", "bush", "buss", "bust", "busy", "buts", "butt", "buys", "buzz", "byte", "cabs", "cafe", "cage", "cake", "calf", "call", "calm", "came", "came", "camp", "cane", "cans", "cant", "cape", "caps", "card", "care", "carp", "cars", "cart", "case", "case", "cash", "cask", "cast", "cats", "cave", "caws", "cede", "cell", "cent", "chap", "char", "chat", "chef", "chem", "chew", "chic", "chin", "chip", "chit", "chop", "chow", "chug", "chum", "cite", "city", "clad", "clan", "clap", "claw", "clay", "clip", "clod", "clog", "clot", "club", "clue", "coal", "coat", "coax", "cobs", "coca", "cock", "coco", "code", "coed", "cogs", "coil", "coin", "cold", "colt", "comb", "come", "cone", "cook", "cool", "coop", "cope", "cops", "copy", "cord", "core", "cork", "corm", "corn", "cost", "cosy", "cots", "cove", "cowl", "cows", "cozy", "crab", "crag", "cram", "crap", "crew", "crib", "crop", "crow", "crux", "cube", "cubs", "cued", "cues", "cuff", "cull", "cult", "cups", "curb", "curd", "cure", "curl", "curs", "curt", "cusp", "cuss", "cute", "cuts", "cyst", "czar", "dace", "dads", "daft", "dale", "dame", "damn", "damp", "dams", "dare", "dark", "darn", "dart", "dash", "data", "date", "dawn", "days", "daze", "dead", "deaf", "deal", "dean", "dear", "debt", "deck", "deed", "deem", "deep", "deer", "deft", "defy", "dell", "demo", "dens", "dent", "deny", "desk", "deus", "dews", "dewy", "dial", "dice", "died", "diem", "dies", "diet", "digs", "dill", "dime", "dims", "dine", "ding", "dint", "dips", "dire", "dirt", "disc", "dish", "disk", "dive", "dock", "doer", "does", "dogs", "dole", "doll", "dolt", "dome", "done", "dons", "doom", "door", "dope", "dose", "dote", "dots", "dove", "down", "doze", "drab", "drag", "dram", "draw", "drew", "drip", "drop", "drug", "drum", "dual", "dubs", "duck", "duct", "dude", "duds", "duel", "dues", "duet", "duke", "dull", "duly", "dumb", "dump", "dune", "dung", "dunk", "dupe", "dusk", "dust", "duty", "dyed", "dyer", "dyes", "each", "each", "earl", "earn", "ears", "ease", "east", "easy", "eats", "ebbs", "echo", "eddy", "edge", "edgy", "edit", "eels", "egad", "eggs", "egos", "eked", "ekes", "elks", "elms", "else", "emit", "ends", "envy", "eons", "epic", "eras", "ergo", "eros", "errs", "etch", "even", "ever", "evil", "ewes", "exam", "exec", "exit", "eyed", "eyer", "eyes", "face", "fact", "fade", "fads", "fail", "fain", "fair", "fake", "fall", "fame", "fang", "fans", "fare", "farm", "faro", "fart", "fast", "fate", "fats", "faun", "fawn", "faze", "fear", "feat", "feed", "feel", "fees", "feet", "fell", "felt", "fend", "fern", "feud", "fiat", "fibs", "fief", "fife", "figs", "file", "fill", "film", "find", "fine", "fink", "fins", "fire", "firm", "fish", "fist", "fits", "five", "fizz", "flab", "flag", "flap", "flat", "flaw", "flax", "flay", "flea", "fled", "flee", "flew", "flex", "flip", "flit", "flog", "flop", "flow", "flue", "flux", "foal", "foam", "foci", "foes", "fogs", "fogy", "foil", "fold", "folk", "fond", "font", "food", "fool", "foot", "fora", "ford", "fore", "fork", "form", "fort", "foul", "four", "fowl", "foxy", "fray", "free", "fret", "frog", "from", "fuel", "full", "fume", "fund", "funk", "furs", "fury", "fuse", "fuss", "fuzz", "gage", "gags", "gain", "gait", "gale", "gall", "gals", "game", "gang", "gape", "gaps", "garb", "gash", "gasp", "gate", "gave", "gawk", "gaze", "gear", "geek", "geld", "gels", "gems", "gene", "gent", "germ", "gets", "gift", "gild", "gill", "gilt", "gins", "gird", "girl", "gist", "give", "glad", "glee", "glen", "glib", "glob", "glow", "glue", "glum", "glut", "gnat", "gnaw", "goad", "goal", "goat", "gobs", "gods", "goes", "gold", "golf", "gone", "gong", "good", "goof", "gore", "gory", "gosh", "gout", "gown", "grab", "grad", "gram", "gray", "grew", "grey", "grid", "grim", "grin", "grip", "grit", "grow", "grub", "gulf", "gull", "gulp", "gums", "guns", "guru", "gush", "gust", "guts", "guys", "gyms", "gyre", "gyro", "hack", "hail", "hair", "hale", "half", "hall", "halt", "hams", "hand", "hang", "hard", "hare", "hark", "harm", "harp", "hash", "hate", "hats", "haul", "have", "hawk", "haze", "hazy", "head", "heal", "heap", "hear", "heat", "heck", "heed", "heel", "heft", "heir", "held", "hell", "helm", "help", "hems", "hens", "herb", "herd", "here", "hero", "hers", "hews", "hick", "hide", "high", "hike", "hill", "hilt", "hind", "hint", "hips", "hire", "hiss", "hits", "hive", "hoar", "hoax", "hobs", "hoes", "hogs", "hold", "hole", "holy", "home", "hone", "honk", "hood", "hoof", "hook", "hoop", "hoot", "hope", "hops", "horn", "hose", "host", "hots", "hour", "hove", "howl", "hubs", "hued", "hues", "huff", "huge", "hugs", "hull", "hump", "hums", "hung", "hunk", "hunt", "hurl", "hurt", "hush", "husk", "huts", "hymn", "ibex", "ibis", "iced", "ices", "icky", "icon", "idea", "idle", "idly", "idol", "iffy", "ills", "imam", "imps", "inch", "inks", "inns", "into", "ions", "iota", "iris", "irks", "iron", "isle", "itch", "item", "jabs", "jack", "jade", "jail", "jams", "jars", "jaws", "jays", "jazz", "jean", "jeep", "jeer", "jerk", "jest", "jets", "jibe", "jigs", "jinx", "jive", "jobs", "jock", "jogs", "join", "joke", "jolt", "jots", "jowl", "joys", "judo", "jugs", "juke", "july", "jump", "june", "junk", "jury", "just", "juts", "keel", "keen", "keep", "kept", "kern", "keys", "kick", "kids", "kill", "kind", "king", "kink", "kiss", "kite", "kits", "knee", "knew", "knit", "knob", "knot", "know", "kudo", "labs", "lace", "lack", "lacy", "lads", "lady", "lags", "laid", "lain", "lair", "lake", "lamb", "lame", "lamp", "land", "lane", "laps", "lard", "lark", "lash", "lass", "last", "late", "lava", "lawn", "laws", "lays", "lazy", "lead", "leaf", "leak", "lean", "leap", "leek", "leer", "left", "legs", "lend", "lens", "lent", "less", "lest", "lets", "levy", "lewd", "liar", "lice", "lick", "lids", "lied", "lies", "lieu", "life", "lift", "like", "lily", "limb", "lime", "limp", "line", "link", "lint", "lion", "lips", "lisp", "list", "live", "load", "loaf", "loan", "lobe", "lobs", "loci", "lock", "lode", "loft", "logo", "logs", "loin", "loll", "lone", "long", "look", "lool", "loom", "loon", "loop", "loot", "lord", "lore", "lose", "loss", "lost", "lots", "loud", "lout", "love", "lows", "luck", "lugs", "lull", "lump", "lung", "lure", "lurk", "lush", "lust", "lute", "lynx", "lyre", "mace", "mace", "mach", "made", "maid", "mail", "maim", "main", "make", "male", "mall", "malt", "mama", "mane", "many", "maps", "mare", "mark", "mars", "mart", "mash", "mask", "mass", "mast", "mate", "math", "mats", "maul", "maze", "mead", "meal", "mean", "meat", "meek", "meet", "melt", "memo", "mend", "menu", "meow", "mere", "mesh", "mess", "meta", "mete", "mets", "mews", "mica", "mice", "mike", "mild", "mile", "milk", "mill", "mime", "mind", "mine", "mini", "mink", "mint", "mire", "miss", "mist", "mite", "mitt", "moan", "moat", "mobs", "mock", "mode", "moho", "mold", "mole", "molt", "moms", "monk", "mood", "moon", "moor", "moot", "mope", "mops", "more", "morn", "moss", "most", "moth", "move", "mows", "much", "muck", "muds", "muff", "mugs", "mule", "mull", "mums", "murk", "muse", "mush", "musk", "must", "mute", "mutt", "myth", "nags", "nail", "name", "naps", "navy", "nays", "near", "neat", "neck", "need", "neon", "nest", "nets", "news", "newt", "next", "nice", "nick", "nigh", "nine", "nips", "node", "nods", "none", "noon", "nope", "norm", "nose", "nosy", "note", "noun", "nude", "null", "numb", "nuns", "nuts", "oaks", "oars", "oath", "oats", "obey", "oboe", "odds", "odes", "odor", "offs", "ohms", "oils", "oily", "oink", "okay", "oldy", "omen", "omit", "omni", "once", "ones", "only", "onto", "onus", "onyx", "oohs", "oops", "ooze", "opal", "open", "opts", "opus", "oral", "orca", "ores", "ouch", "ours", "oust", "outs", "oval", "oven", "over", "ovum", "owed", "owes", "owls", "owns", "oxen", "pace", "pack", "pact", "pads", "page", "paid", "pail", "pain", "pair", "pale", "pall", "palm", "pals", "pane", "pang", "pans", "pant", "papa", "pare", "park", "pars", "part", "pass", "past", "pate", "path", "pats", "pave", "pawn", "paws", "pays", "peak", "peal", "pear", "peas", "peat", "peck", "peek", "peel", "peep", "peer", "pegs", "pelt", "pend", "pens", "pent", "peon", "perk", "pert", "peso", "pest", "pets", "pews", "phat", "pica", "pick", "pied", "pier", "pies", "pigs", "pike", "pile", "pill", "pimp", "pine", "ping", "pink", "pins", "pint", "pipe", "pips", "pith", "pits", "pity", "plan", "play", "plea", "plod", "plop", "plot", "plow", "ploy", "plug", "plum", "plus", "pock", "pods", "poem", "poet", "pogo", "poke", "pole", "poll", "polo", "pomp", "pond", "pone", "pong", "pony", "pool", "poop", "poor", "pope", "pops", "pore", "pork", "port", "pose", "posh", "post", "pots", "pour", "pout", "pray", "prep", "prey", "prim", "prod", "prop", "pros", "prow", "pubs", "puck", "puff", "puke", "pull", "pulp", "puma", "pump", "punk", "puns", "punt", "puny", "pupa", "pups", "pure", "purr", "push", "puts", "putt", "pyre", "quad", "quit", "quiz", "race", "race", "rack", "racy", "raft", "rage", "rags", "raid", "rail", "rain", "rake", "ramp", "rams", "rang", "rank", "rant", "rape", "raps", "rapt", "rare", "rash", "rasp", "rate", "rats", "rave", "rays", "raze", "read", "real", "ream", "reap", "rear", "redo", "reds", "reed", "reef", "reel", "rein", "rely", "rend", "rent", "rest", "ribs", "rice", "rich", "ride", "rids", "rift", "rigs", "rime", "rims", "rind", "ring", "rink", "riot", "ripe", "rips", "rise", "risk", "rite", "road", "roam", "roar", "robe", "robs", "rock", "rode", "rods", "role", "roll", "romp", "roof", "rook", "room", "root", "rope", "rose", "rosy", "rots", "rout", "rove", "rows", "rubs", "ruby", "rude", "ruff", "rugs", "ruin", "rule", "rums", "rung", "runs", "runt", "rush", "rust", "ruts", "sack", "safe", "saga", "sage", "sags", "said", "sail", "sake", "sale", "salt", "same", "sand", "sane", "sang", "sank", "saps", "sash", "sate", "save", "saws", "says", "scab", "scam", "scan", "scar", "scat", "scow", "scud", "scum", "seal", "seam", "sear", "seas", "seat", "sect", "seed", "seek", "seem", "seen", "seep", "seer", "sees", "seif", "self", "sell", "semi", "send", "sent", "sept", "serf", "sets", "sewn", "sews", "sexy", "sham", "shed", "shin", "ship", "shod", "shoe", "shop", "shot", "show", "shun", "shut", "sick", "side", "sift", "sigh", "sign", "silk", "sill", "silo", "silt", "sine", "sing", "sink", "sins", "sips", "sire", "sirs", "site", "sits", "situ", "size", "skew", "skid", "skim", "skin", "skip", "skis", "skit", "slab", "slam", "slap", "slat", "slay", "sled", "slew", "slid", "slim", "slip", "slit", "slob", "slop", "slot", "slow", "slug", "slum", "slur", "smog", "smug", "smut", "snag", "snap", "snip", "snob", "snot", "snow", "snub", "snug", "soak", "soap", "soar", "sobs", "sock", "soda", "sods", "sofa", "soft", "soil", "sold", "sole", "solo", "some", "song", "sons", "soon", "soot", "sops", "sore", "sort", "soul", "soup", "sour", "sown", "sows", "soya", "span", "spas", "spat", "sped", "spin", "spit", "spot", "spry", "spud", "spun", "spur", "stab", "stag", "star", "stat", "stay", "stem", "step", "stew", "stir", "stop", "stow", "stub", "stud", "stun", "subs", "such", "suck", "suds", "sued", "sues", "suit", "sulk", "sums", "sung", "sunk", "suns", "sure", "surf", "swab", "swam", "swan", "swap", "swat", "sway", "swig", "swim", "swum", "sync", "tabs", "tack", "taco", "tact", "tags", "tail", "take", "talc", "tale", "talk", "tall", "tame", "tamp", "tank", "tape", "taps", "tart", "task", "taut", "taxi", "teal", "team", "tear", "teas", "teem", "teen", "tees", "tell", "tend", "tens", "tent", "term", "tern", "test", "text", "than", "that", "thaw", "thee", "them", "then", "they", "thin", "this", "thou", "thru", "thud", "thug", "thus", "tick", "tics", "tide", "tidy", "tied", "tier", "ties", "tile", "till", "tilt", "time", "tins", "tint", "tiny", "tips", "tire", "toad", "toed", "toes", "tofu", "toga", "toil", "told", "toll", "tomb", "tone", "tons", "took", "tool", "toot", "tops", "tore", "torn", "toss", "tote", "tots", "tour", "tout", "town", "tows", "toys", "tram", "trap", "tray", "tree", "trek", "trim", "trio", "trip", "trod", "trot", "tube", "tubs", "tuck", "tuff", "tuft", "tugs", "tuna", "tune", "turf", "turn", "tusk", "twig", "twin", "twos", "type", "typo", "ugly", "undo", "unit", "unto", "upon", "urea", "urge", "urns", "used", "user", "uses", "vain", "vale", "vamp", "vane", "vans", "vary", "vase", "vast", "vats", "veal", "veer", "veil", "vein", "vent", "verb", "very", "vest", "veto", "vets", "vial", "vibe", "vice", "vied", "vies", "view", "vile", "vine", "visa", "vita", "void", "volt", "vote", "vows", "wade", "waft", "wage", "wags", "wail", "wait", "wake", "walk", "wall", "wand", "wane", "want", "ward", "ware", "warm", "warn", "warp", "wars", "wart", "wary", "wash", "wasp", "wave", "waxy", "ways", "weak", "wean", "wear", "webs", "weds", "weed", "week", "weep", "weld", "well", "wend", "went", "wept", "were", "west", "wets", "wham", "what", "when", "whey", "whim", "whip", "whir", "whiz", "whom", "wick", "wide", "wife", "wigs", "wild", "wile", "will", "wilt", "wily", "wind", "wine", "wing", "wink", "wins", "wipe", "wire", "wiry", "wise", "wish", "wisp", "with", "wits", "woes", "woke", "woks", "wolf", "womb", "wont", "wood", "woof", "wool", "woos", "word", "wore", "work", "worm", "worn", "wove", "wows", "wrap", "wren", "writ", "xray", "yams", "yank", "yaps", "yard", "yarn", "yawn", "year", "yell", "yelp", "yeti", "yoga", "yoke", "yolk", "your", "yoyo", "yuck", "zany", "zaps", "zeal", "zero", "zest", "zeta", "zinc", "zing", "zips", "zone", "zoom", "zoos"];
var indices = [0, 70, 220, 342, 454, 498, 608, 698, 799, 825, 863, 885, 995, 1100, 1136, 1184, 1325, 1328, 1424, 1621, 1737, 1748, 1781, 1873, 1874, 1890, 1902];
var wLength = words.length;
var math = require('mathjs');
var HashMap = require('hashmap');

var RtmClient = require('@slack/client').RtmClient;

var MemoryDataStore = require('@slack/client').MemoryDataStore;

var token = process.env.WORD_RUSH_SLACK_BOT || '';

var map = new HashMap();

var RTM_EVENTS = require('@slack/client').RTM_EVENTS;


var rtm = new RtmClient(token, {
    logLevel: 'error',
    dataStore: new MemoryDataStore()
});
rtm.start();

rtm.on(RTM_EVENTS.MESSAGE, function handleRtmMessage(message) {
    console.log('Message:', message.text);
    console.log('Message User:', message.user);
    console.log('Name :', rtm.dataStore.getUserById(message.user).name);
    message.text = message.text.toLowerCase().trim();
    var userData = map.get(message.user)
    if (userData == null || userData == undefined || userData.inGame == false) {

        if (message.text.search("hey") != -1 || message.text.search("hello") != -1 || message.text.search("hi") != -1) {
            rtm.sendMessage("Sup! " + rtm.dataStore.getUserById(message.user).name + " !", message.channel);
        } else if (message.text.search("play") != -1 || message.text.search("Play") != -1) {
            rtm.sendMessage("Sup! " + rtm.dataStore.getUserById(message.user).name + " !", message.channel);
            rtm.sendMessage("Let's play Word Rush", message.channel);
            rtm.sendMessage("Find words using only these letters", message.channel);
            rtm.sendMessage("*i*,*e*,*h*,*k*", message.channel);
            map.set(message.user, {
                inGame: true,
                score: 0,
                currentWord: "hike"
            });
        } else {
            rtm.sendMessage("I am not that smart :stuck_out_tongue_winking_eye:", message.channel);
            rtm.sendMessage("Type 'play' to play.", message.channel);
        }
    } else {
        if (userData.inGame == true) {
            var firstCharIndex = message.text[0].charCodeAt() - 'a'.charCodeAt();
            var correct = false;
            if (message.text.length == 4) {
                var stringArray1 = [message.text[0], message.text[1], message.text[2], message.text[3]];
                var stringArray2 = [userData.currentWord[0], userData.currentWord[1], userData.currentWord[2], userData.currentWord[3]];
                stringArray1.sort();
                stringArray2.sort();
                var sameLetters = true;
                for (var i = 0; i < 4; i++) {
                    if (stringArray1[i] != stringArray2[i]) {
                        sameLetters = false;
                        break;
                    }
                }
                if (sameLetters) {
                    for (var i = indices[firstCharIndex]; i < indices[firstCharIndex + 1]; i++) {
                        if (words[i] === message.text) {
                            correct = true;
                            rtm.sendMessage("Correct Answer :smile:", message.channel);
                            userData.currentWord = words[parseInt(math.floor(math.random() * wLength))];
                            userData.score += 1;
                            rtm.sendMessage("Current Score : " + userData.score, message.channel);
                            rtm.sendMessage("Next Word", message.channel);
                            var order = [];
                            var count = 0;
                            while (true) {
                                count++;
                                if (count == 100) {
                                    order = [3, 1, 2, 0];
                                    break;
                                } else {
                                    var number = parseInt(math.floor(math.random() * 4));
                                    if (order.indexOf(number) == -1) {
                                        order.push(number);
                                        if (order.length == 4) {
                                            break;
                                        }
                                    }
                                }
                            }
                            rtm.sendMessage("*" + userData.currentWord[order[0]] + "*,*" + userData.currentWord[order[1]] + "*,*" + userData.currentWord[order[2]] + "*,*" + userData.currentWord[order[3]] + "*", message.channel);
                            break;
                        }
                    }
                }

            }
            if (correct === false) {
                rtm.sendMessage("Sorry, Wrong Answer. :white_frowning_face: ", message.channel);
                rtm.sendMessage("Your final score : " + userData.score, message.channel);
                rtm.sendMessage("Correct answer was : *" + userData.currentWord+"*", message.channel);
                if (userData.score <= 10) {
                    rtm.sendMessage("You can do better, much much better", message.channel);
                } else if (userData.score > 10 && userData.score <= 15) {
                    rtm.sendMessage("Acceptable! You are smarter than the fifth grader", message.channel);
                } else if (userData.score > 15 && userData.score <= 25) {
                    rtm.sendMessage("You are awesome! well, almost. ", message.channel);
                } else if (userData.score > 25 && userData.score <= 50) {
                    rtm.sendMessage("I am impressed. Really!", message.channel);
                } else {
                    rtm.sendMessage("Get back to work! Like seriously! Now!", message.channel);
                }
                userData.inGame = false;
                rtm.sendMessage("---", message.channel);
                rtm.sendMessage("Type 'play' to play again.", message.channel);
                rtm.sendMessage("---", message.channel);
            }
        }
    }
});